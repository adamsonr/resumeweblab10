var mysql   = require('mysql');
var db  = require('./db_connection.js');

/* DATABASE CONFIGURATION */
var connection = mysql.createConnection(db.config);

exports.getAll = function(callback) {
    var query = 'select resume_id, resume_name, first_name, last_name from resume '+
        'join account on account.account_id = resume.account_id order by first_name';

    connection.query(query, function(err, result) {
        callback(err, result);
    });


};

exports.getAccounts = function(callback) {
    var query = 'select * from account order by first_name';

    connection.query(query, function(err, result) {
        callback(err, result);
    });


};

exports.getById = function(resume_id, callback) {
    var query = 'select resume_id, resume_name, first_name, last_name from resume '+
    'join account on account.account_id = resume.account_id ' +
    'where resume_id = ?';
    var queryData = [resume_id];
    console.log(query);

    connection.query(query, queryData, function(err, result) {

        callback(err, result);
    });
};

exports.edit = function(account_id, callback) {
    var query = 'CALL account_getUserInfo(?)';
    var queryData = [account_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};

exports.edit2 = function(resume_id, callback) {
    var query = 'CALL resume_getResumeInfo(?)';
    var queryData = [resume_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};

//declare the functions so they can be used locally
var resumeDeleteAll = function(resume_id, callback){
    var query = 'DELETE FROM resume_school WHERE resume_id = ?';
    var queryData = [resume_id];

    connection.query(query, queryData, function(err, result) {
        var query = 'DELETE FROM resume_company WHERE resume_id = ?';
        var queryData = [resume_id];

        connection.query(query, queryData, function(err, result) {
            var query = 'DELETE FROM resume_skill WHERE resume_id = ?';
            var queryData = [resume_id];

            connection.query(query, queryData, function(err, result) {
                callback(err, result);
            });
        });
    });
};

var resumeInsertAll = function(resume_id, schoolIdArray, companyIdArray, skillIdArray, callback){
    var noSchool = false;
    var noCompany = false;
    var noSkill = false;
    // NOTE THAT THERE IS ONLY ONE QUESTION MARK IN VALUES ?
    var query = 'INSERT INTO resume_school (resume_id, school_id) VALUES ?';
    if(schoolIdArray == null)
        noSchool = true;

    // TO BULK INSERT RECORDS WE CREATE A MULTIDIMENSIONAL ARRAY OF THE VALUES
    var schoolData = [];
    if(!noSchool) {
        for (var i = 0; i < schoolIdArray.length; i++) {
            schoolData.push([resume_id, schoolIdArray[i]]);
        }
    }
    else{
        schoolData.push([resume_id]);
        query = 'select * from resume where resume_id = ?';
    }
    connection.query(query, [schoolData], function(err, result){
        // NOTE THAT THERE IS ONLY ONE QUESTION MARK IN VALUES ?
        var query = 'INSERT INTO resume_company (resume_id, company_id) VALUES ?';
        if(companyIdArray == null)
            noCompany = true;

        // TO BULK INSERT RECORDS WE CREATE A MULTIDIMENSIONAL ARRAY OF THE VALUES
        var companyData = [];
        if(!noCompany) {
            for (var i = 0; i < companyIdArray.length; i++) {
                companyData.push([resume_id, companyIdArray[i]]);
            }
        }
        else{
            companyData.push([resume_id]);
            query = 'select * from resume where resume_id = ?';
        }

        connection.query(query, [companyData], function(err, result){
            // NOTE THAT THERE IS ONLY ONE QUESTION MARK IN VALUES ?
            var query = 'INSERT INTO resume_skill (resume_id, skill_id) VALUES ?';
            if(skillIdArray == null)
                noSkill = true;

            // TO BULK INSERT RECORDS WE CREATE A MULTIDIMENSIONAL ARRAY OF THE VALUES
            var skillData = [];
            if(!noSkill) {
                for (var i = 0; i < skillIdArray.length; i++) {
                    skillData.push([resume_id, skillIdArray[i]]);
                }
            }
            else{
                skillData.push([resume_id]);
                query = 'select * from resume where resume_id = ?';
            }
            connection.query(query, [skillData], function(err, result){
                callback(err, result);
            });
        });
    });
};


//export the same functions so they can be used by external callers
module.exports.resumeDeleteAll = resumeDeleteAll;
module.exports.resumeInsertAll = resumeInsertAll;

exports.update = function(params, callback) {
    var query = 'UPDATE resume SET resume_name = ? WHERE resume_id = ?';
    var queryData = [params.resume_name, params.resume_id];

    connection.query(query, queryData, function(err, result) {
        //delete all the school, company, and skill entries for this resume
        resumeDeleteAll(params.resume_id, function(err, result){
            //insert all the school, company, and skill entries for this resume
            resumeInsertAll(params.resume_id, params.school_id, params.company_id, params.skill_id, function(err, result){
                callback(err, params.resume_id);
            });
        });

    });
};

exports.insert = function(params, callback) {

    // FIRST INSERT THE RESUME
    var query = 'INSERT INTO resume (account_id, resume_name) VALUES (?, ?)';

    var queryData = [params.account_id, params.resume_name];

    connection.query(query, queryData, function(err, result) {

        // THEN USE THE RESUME_ID RETURNED AS insertId
        var resume_id = result.insertId;

        // insert school
        var query = 'INSERT INTO resume_school (resume_id, school_id) VALUES ?';

        // TO BULK INSERT RECORDS WE CREATE A MULTIDIMENSIONAL ARRAY OF THE VALUES
        var resumeSchoolData = [];
        for(var i=0; i < params.school_id.length; i++) {
            resumeSchoolData.push([resume_id, params.school_id[i]]);
        }

        // NOTE THE EXTRA [] AROUND companyAddressData
        connection.query(query, [resumeSchoolData], function(err, result){

            // insert company
            var query = 'INSERT INTO resume_company (resume_id, company_id) VALUES ?';

            // TO BULK INSERT RECORDS WE CREATE A MULTIDIMENSIONAL ARRAY OF THE VALUES
            var resumeCompanyData = [];
            for(var i=0; i < params.school_id.length; i++) {
                resumeCompanyData.push([resume_id, params.company_id[i]]);
            }

            // NOTE THE EXTRA [] AROUND companyAddressData
            connection.query(query, [resumeCompanyData], function(err, result){
                // insert skill
                var query = 'INSERT INTO resume_skill (resume_id, skill_id) VALUES ?';

                // TO BULK INSERT RECORDS WE CREATE A MULTIDIMENSIONAL ARRAY OF THE VALUES
                var resumeSkillData = [];
                for(var i=0; i < params.skill_id.length; i++) {
                    resumeSkillData.push([resume_id, params.skill_id[i]]);
                }

                // NOTE THE EXTRA [] AROUND companyAddressData
                connection.query(query, [resumeSkillData], function(err, result){
                    callback(err, resume_id);
                });
            });
        });




    });

};

exports.delete = function(resume_id, callback) {
    var query = 'DELETE FROM resume WHERE resume_id = ?';
    var queryData = [resume_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });

};