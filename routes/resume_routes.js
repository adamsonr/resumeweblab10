var express = require('express');
var router = express.Router();
var resume_dal = require('../model/resume_dal');
var resume_dal = require('../model/resume_dal');


// View All resumes
router.get('/all', function(req, res) {
    resume_dal.getAll(function(err, result){
        if(err) {
            res.send(err);
        }
        else {
            res.render('resume/resumeViewAll', { 'result':result });
        }
    });

});

// Select user
router.get('/add/selectuser', function(req, res) {
    resume_dal.getAccounts(function(err,result) {
        if (err) {
            res.send(err);
        }
        else {
            res.render('resume/resumeSelectUser', {'result': result});
        }
    });

});
// View the resume for the given id
router.get('/', function(req, res){
    if(req.query.resume_id == null) {
        res.send('resume_id is null');
    }
    else {
        resume_dal.getById(req.query.resume_id, function(err,result) {
            if (err) {
                res.send(err);
            }
            else {
                res.render('resume/resumeViewById', {'result': result});
            }
        });
    }
});



router.get('/add', function(req, res){
    if(req.query.account_id == null) {
        res.send('An account id is required');
    }
    else {
        resume_dal.edit(req.query.account_id, function(err, result){
            res.render('resume/resumeAdd', {account: result[0][0], school: result[1], company: result[2], skill: result[3]});
        });
    }

});

// View the resume for the given id
router.post('/insert', function(req, res){

    // passing all the query parameters (req.query) to the insert function instead of each individually
    resume_dal.insert(req.body, function(err,resume_id) {
        if (err) {
            console.log(err)
            res.send(err);
        }
        else {
            //poor practice for redirecting the user to a different page, but we will handle it differently once we start using Ajax
            resume_dal.edit2(resume_id, function(err, result){
                res.render('resume/resumeUpdate', {resume: result[0][0], school: result[1], company: result[2], skill: result[3], was_successful: true});
            })
        }
    });
});

router.get('/edit', function(req, res){
    if(req.query.resume_id == null) {
        res.send('A resume id is required');
    }
    else {
        resume_dal.edit2(req.query.resume_id, function(err, result){
            res.render('resume/resumeUpdate', {resume: result[0][0], school: result[1], company: result[2], skill: result[3]});
        });
    }

});

/*router.get('/edit2', function(req, res){
 if(req.query.resume_id == null) {
 res.send('A resume id is required');
 }
 else {
 resume_dal.getById(req.query.resume_id, function(err, resume){
 resume_dal.getAll(function(err, resume) {
 res.render('resume/resumeUpdate', {resume: resume[0], resume: resume});
 });
 });
 }

 });*/

router.get('/update', function(req, res) {
    resume_dal.update(req.query, function(err, resume_id){
        resume_dal.edit2(resume_id, function(err, result){
            res.render('resume/resumeUpdate', {resume: result[0][0], school: result[1], company: result[2], skill: result[3], was_successful: true});
        })
    });
});

// Delete a resume for the given resume_id
router.get('/delete', function(req, res){
    if(req.query.resume_id == null) {
        res.send('resume_id is null');
    }
    else {
        resume_dal.delete(req.query.resume_id, function(err, result){
            if(err) {
                res.send(err);
            }
            else {
                //poor practice, but we will handle it differently once we start using Ajax
                res.redirect(302, '/resume/all');
            }
        });
    }
});

module.exports = router;
